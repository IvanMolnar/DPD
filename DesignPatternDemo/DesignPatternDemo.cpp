// DesignPatternDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "GameLogic.h"

int main()
{
	GameLogic l;
	l.run();

    return 0;
}

